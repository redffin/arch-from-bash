#!/bin/bash


echo "Hello! Let's DO!!!"

# Make dir and replase config

mkdir "$HOME"/.config
mv .config/* "$HOME"/.config/


# Aura

git clone https://aur.archlinux.org/aura-bin.git
cd aura-bin || exit 
makepkg -si


# Rate-mirrors
sudo aura -A rate-mirrors-bin
rate-mirrors arch

# Arch keys

sudo pacman-key --init               
sudo pacman-key --populate archlinux
sudo pacman-key --refresh-keys
sudo pacman -Sy



# AMD stuff

sudo aura -Auy


# Main pack

sudo aura -Syu --needed git nano keepassxc neovim flatpak \
    qbittorrent timeshift linux-lts linux-lts-headers \
    libreoffice-fresh-ru libreoffice-fresh hyprland waybar \
    wofi mako mpv gparted nemo fastfetch swaybg swayimg \
    telegram-desktop mousepad ark capitaine-cursors \
    gnome-system-monitor kitty materia-gtk-theme neofetch \
    pamixer papirus-icon-theme pavucontrol polkit-gnome \
    swaylock ttf-iosevka-nerd zsh zsh-syntax-highlighting \
    wl-clipboard alsa-tools alsa-utils haveged cliphist \



# AUR pack

sudo aura -A light vscodium-bin freetube-bin ttf-times-new-roman \
    auto-cpufreq ananicy-cpp hyprshot wlsunset librewolf-bin


# Systemd enable

sudo systemctl enable --now auto-cpufreq
sudo systemctl enable --now ananicy-cpp
sudo systemctl enable haveged
sudo systemctl enable fstrim.timer
sudo fstrim -v /

sudo usermod -aG video redf

# and more

# Questions about install pack
# 1. CUPS and scan

echo "Do you want to install printes and scan stuff(1=yes / 0=no)"
otvet_1=1
read -r otvet_1

if [ "$otvet_1" = "1" ]; then
    echo "OK!"
    sudo aura -S cups scan hplip simple-scan

    sudo systemctl enable cups
    sudo systemctl start cups

    echo Dont forget use "hp-setup -i"
fi


# 2. Virtmanager

echo "Do you want to install Virtmanager (1=yes / 0=no)"
otvet_2=1
read -r otvet_2

if [ "$otvet_2" = "1" ]; then
    echo "OK!!"
    sudo pacman -S virt-manager qemu bridge-utils dnsmasq

    sudo usermod -aG kvm "$USER"
    sudo usermod -aG libvirt "$USER"

    sudo systemctl enable libvirtd.service
fi

# 3. Portproton (необходим multulib)
echo "Do you want to install portproton(need multulib repo) (1=yes / 0=no)"
otvet_3=1
read -r otvet_3

if [ "$otvet_3" -eq "1" ]; then
    echo "OK!"
    sudo aura -A portproton
fi


# 4. Flatpak pack
echo "Do you want to install flatpak pack? (1=yes / 0=no) "
otvet_4=1
read -r otvet_4

if [ "$otvet_4" -eq "1" ]; then
    echo "All Right!"

    flatpak install flathub org.kde.krita org.kde.kdenlive \
    com.github.tchx84.Flatseal io.github.thetumultuousunicornofdarkness.cpu-x \
    com.librumreader.librum com.logseq.Logseq eu.betterbird.Betterbird \
    org.fedoraproject.MediaWriter org.gnome.baobab

fi

# virtmanager caps and another

echo "It's OVER!"
echo "Please reboot now..."
echo "REBOOT? {1=yes / 0=no}"

otvet_5=1
read -r otvet_5

if [ "$otvet_5" -eq "1" ]; then
    echo "Bye!"
    reboot
fi